<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="g" %>
<table border="1px solid black">
	<tr>
		<th>No</th>
		<th>Item Name</th>
		<th>Item Price</th>
		<th>Item Quantity</th>
	</tr>
	<g:forEach var="orderItem" items="${sessionScope.orderItem}" varStatus="j">
		<tr>	
			<td>${j.count}</td>
			<td>${orderItem.itemName}</td>
			<td>${orderItem.itemPrice}</td>
			<td>${orderItem.quantity}</td>
		</tr>
	</g:forEach>
	</table>
	
</body>
</html>