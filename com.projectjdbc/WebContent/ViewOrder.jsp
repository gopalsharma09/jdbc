<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="OrderController" method=post>
	Sort Filed:<select name="sortFiled">
			<option value="select" disabled>select</option>
			<option value="order_id">Order Id</option>
			<option value="order_date">Order Date</option>
			</select>
	Sorting Order:<select name="sortOrder">
			<option value="select" disabled>select</option>
			<option value="ASC">ASC</option>
			<option value="DESC">DESC</option>
			</select>
	Page size:<input type="text" name="pageSize">
	<input type="hidden" name="loaded" value="true">
	<input type="hidden" name="pageNo" value="1">
	<input type="submit" name="flag" value="viewOrder"><br></br>

 
</form>

<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="g" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt"%>
	<g:set var="pageNo" value="${sessionScope.pageNo}"></g:set>
	<fmt:parseNumber var = "i" integerOnly = "true" type = "number" value = "${pageNo}"></fmt:parseNumber>
	<table border="1px solid black">
	<tr>
		<th>No</th>
		<th>Order Id</th>
		<th>Order Date</th>
		<th>Order Details</th>
		<th>Cancel Order</th>
	</tr>
	<g:forEach var="order" items="${sessionScope.order}" varStatus="j">
		<tr>	
			<td>${j.count}</td>
			<td>${order.orderId}</td>
			<td>${order.date}</td>
			<td><a href="OrderController?flag=cancelOrder&orderId=${order.orderId}">Cancel Order</a></td>
			<td><a href="OrderController?flag=viewOrderItem&orderId=${order.orderId}">View</a></td>
		</tr>
	</g:forEach>
	</table>
	<g:if test = "${i>1}">
    	<a href="OrderController?flag=viewOrder&pageNo=${i-1}"><button>previous</button></a>     
    </g:if>
    <g:out value="Page:${i}"></g:out>
	<a href="OrderController?flag=viewOrder&pageNo=${i+1}"><button>next</button></a>
</body>
</body>
</html>