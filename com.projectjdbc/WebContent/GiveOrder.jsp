<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="OrderController" method="post">
	<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="g" %>
		<table border="1px solid black">
			<tr>
				<td>No</td>
				<td>Food Name</td>
				<td>Food Price</td>
				<td>Select</td>
				<td>Quantity</td>
			</tr>
	<g:forEach var="item" items="${sessionScope.listOfItem}" varStatus="j">
	<tr>
		<td>${j.count}</td>
		<td>${item.itemName}</td>
		<td>${item.itemPrice}</td>
		<td><input type="checkbox" name="itemOrder" value="${item.itemId}"></td>
		<td><input type="text" name="itemQuantity"></td>
	</tr>
	</g:forEach>
	</table>
	<br></br>
	<input type="submit" name="flag" value="order">
	<br></br>
	
</form>

</body>
</html>