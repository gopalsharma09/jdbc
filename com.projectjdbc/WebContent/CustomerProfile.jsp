<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="g" %>
	<table border="1px solid black">
	<tr>
		<th>Customer Id</th>
		<th>First Name</th>
		<th>Last Price</th>
		<th>Phone Number</th>
		<th>Edit Profile</th>
		<th>Delete Account</th>
	</tr>
	<g:forEach var="customer" items="${sessionScope.customerDetails}">
		<tr>	
			<td>${customer.customerId}</td>
			<td>${customer.firstName}</td>
			<td>${customer.lastName}</td>
			<td>${customer.phoneNumber}</td>
			<td><a href="CustomerController?flag=editCustomerDetails&customerId=${customer.customerId}">Edit</a></td>
			<td><a href="CustomerController?flag=deleteCustomerAccount&customerId=${customer.customerId}">Delete</a></td>
		</tr>
	</g:forEach>
	</table>
	
</body>
</html>