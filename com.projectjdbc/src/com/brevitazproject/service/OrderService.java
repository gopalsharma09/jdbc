package com.brevitazproject.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.brevitazproject.dao.OrderDao;
import com.brevitazproject.model.Item;
import com.brevitazproject.model.Order;
import com.brevitazproject.model.OrderItem;
import com.brevitazproject.model.Page;
import com.brevitazproject.model.Sort;

public class OrderService {
	
	private OrderDao orderDao=new OrderDao();
	
	public List<Item> getMenu() throws SQLException {
		List<Item> listOfItem=orderDao.getMenu();
		return listOfItem;
	}

	public Order saveOrder(Order order) throws SQLException {
		List<OrderItem> orderItems=new ArrayList<OrderItem>();
		orderDao.saveOrder(order);
		order=orderDao.getOrder(order);
		List<OrderItem> listOfOrderItem=order.getListOfOrderItem();
		for(OrderItem orderItem:listOfOrderItem)
		{
			Item item=new Item();
			item.setItemId(orderItem.getItemId());
			item=orderDao.getItemDetails(item);
			orderItem.setItemName(item.getItemName());
			orderItem.setItemPrice(item.getItemPrice());
			orderItem.setQuantity(orderItem.getQuantity());
			orderItem.setOrder(order);
			orderItems.add(orderItem);
		}
		orderDao.saveOrderDetails(orderItems);
		return order;
	}

	public List<Order> getAllOrder(int customerId, Page page, Sort sort) throws SQLException {
		List<Order> listOfOrder=orderDao.getAllOrder(customerId,page,sort);
		return listOfOrder;
	}
	public List<OrderItem> getAllOrderItem(int orderId) throws SQLException {
		List<OrderItem> listOfOrderItem=orderDao.getAllOrderItem(orderId);
		return listOfOrderItem;
	}
	
	public List<OrderItem> searchOrderItem(int orderId) throws SQLException {
		List<OrderItem> listOfOrderItem=orderDao.searchOrderItem(orderId);
		return listOfOrderItem;
	}

	public void removeItem(int orderId, int itemId) throws SQLException {
		orderDao.deleteItem(orderId,itemId);
	}

	public void cancleOrder(int orderId) throws SQLException {
		orderDao.deleteOrder(orderId);
	}

	public List<Order> searchOrder(int customerId, Page page, Sort sort) throws SQLException {
		List<Order> listOfOrder=orderDao.searchOrder(customerId,page,sort);
		return listOfOrder;
	}
}

