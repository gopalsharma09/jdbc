package com.brevitazproject.service;

import java.sql.SQLException;
import java.util.*;

import com.brevitazproject.dao.CustomerDao;
import com.brevitazproject.model.Customer;

public class CustomerService {

	CustomerDao customerDao=new CustomerDao();
	
	public List<Customer> saveCustomerDetails(Customer customer) throws ClassNotFoundException, SQLException {
		
		List<Customer> customerDetails=new ArrayList<>();
		boolean temp=customerDao.authenticateCustomer(customer);
		if(temp==true)
			customer=customerDao.getCustomer(customer);
		else
		{	customerDao.saveCustomer(customer);
			customer=customerDao.getCustomer(customer);
		}
		customerDetails.add(customer);
		return customerDetails;
	}

	public boolean searchCustomer(int customerId) throws SQLException {
		boolean customer=customerDao.searchCustomer(customerId);
		return customer;
	}

	public void deleteCustomerAccount(int customerId) throws SQLException {
		customerDao.deleteCustomerDetails(customerId);
	}

	public List<Customer> getCustomerDetails(int customerId) throws SQLException {
		List<Customer> customer=customerDao.getCustomerDetails(customerId);
		return customer;
	}

	public void updateCustomerDetails(Customer customer) throws SQLException {
		customerDao.updateCustomerDetails(customer);
	}
}
