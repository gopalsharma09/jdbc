package com.brevitazproject.controller;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.brevitazproject.model.Customer;
import com.brevitazproject.model.Item;
import com.brevitazproject.model.Order;
import com.brevitazproject.model.OrderItem;
import com.brevitazproject.model.Page;
import com.brevitazproject.model.Sort;
import com.brevitazproject.service.OrderService;

@WebServlet("/OrderController")
public class OrderController extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	private OrderService orderService=new OrderService();
	
    public OrderController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		ServletContext application=getServletContext();
		response.setContentType("text/html");
		String flag=request.getParameter("flag");
		if(flag.equals("getMenu"))
		
		{
			try {
				getMenu(request,response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("viewOrder"))
		{
			try {
				viewOrder(request,response,application);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("viewOrderItem"))
		{
			try {
				viewOrderItem(request,response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("removeItem"))
		{
			try {
				removeItem(request,response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("searchOrder"))
		{
			try {
				searchOrder(request,response,application);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("searchOrderItem"))
		{
			try {
				searchOrderItem(request,response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("cancelOrder"))
		{
			try {
				cancelOrder(request,response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		ServletContext application=getServletContext();
		String flag=request.getParameter("flag");
		if(flag.equals("order"))
		{
			try {
				placeOrder(request,response,application);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("searchOrder"))
		{
			try {
				searchOrder(request,response,application);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("viewOrder"))
		{
			try {
				viewOrder(request,response,application);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void getMenu(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		HttpSession session=request.getSession();
		List<Item> listOfIteam=orderService.getMenu();
		session.setAttribute("listOfItem",listOfIteam);
		response.sendRedirect("GiveOrder.jsp");
	}
	
	private void placeOrder(HttpServletRequest request, HttpServletResponse response, ServletContext application) throws SQLException, IOException {
		
		int[] itemId=Stream.of(request.getParameterValues("itemOrder")).mapToInt(Integer::parseInt).toArray();
		int[] itemQuantity=Stream.of(request.getParameterValues("itemQuantity")).filter(value->!value.isEmpty()).
				mapToInt(Integer::parseInt).toArray();
		List<OrderItem> listOfOrderItem=new ArrayList<OrderItem>();
		for(int i=0;i<itemId.length;i++)
		{
			OrderItem orderItem=new OrderItem();
			orderItem.setItemId(itemId[i]);
			orderItem.setQuantity(itemQuantity[i]);
			listOfOrderItem.add(orderItem);
		}
		long millis=System.currentTimeMillis();
		Date date=new Date(millis);
		int customerId=(int)application.getAttribute("customerId");
		
		Customer customer=new Customer();
		Order order=new Order();
		
		customer.setCustomerId(customerId);
		order.setCustomer(customer);
		order.setDate(date);
		order.setListOfOrderItem(listOfOrderItem);
		order=orderService.saveOrder(order);
		application.setAttribute("orderId", order.getOrderId());
		
		response.sendRedirect("OrderController?flag=viewOrder&pageNo=1");
	}

	private void viewOrder(HttpServletRequest request, HttpServletResponse response, ServletContext application) throws SQLException, IOException {
		HttpSession session=request.getSession();
		Page page=new Page();
		Sort sort=new Sort();
		
		int customerId=(int)application.getAttribute("customerId");
		int pageNo=Integer.parseInt(request.getParameter("pageNo"));
		
		String sortFiled=sort.getSortFiled();
		String sortOrder=sort.getSortOrder();
		int pageSize=page.getSize();
		if("true".equals(request.getParameter("loaded")))
		{
			sortFiled=request.getParameter("sortFiled");
			sortOrder=request.getParameter("sortOrder");
			if(!request.getParameter("pageSize").isEmpty())
				pageSize=Integer.parseInt(request.getParameter("pageSize"));
			session.setAttribute("sortFiledView",sortFiled);
			session.setAttribute("sortOrderView",sortOrder);
			session.setAttribute("pageSizeView",pageSize);
		}
		else
		{
			if(session.getAttribute("sortFiled")!=null)
			{
				sortFiled=(String)session.getAttribute("sortFiledView");
				sortOrder=(String)session.getAttribute("sortOrderView");
				pageSize=(int)session.getAttribute("pageSizeView");
			}
		}
		sort.setSortFiled(sortFiled);
		sort.setSortOrder(sortOrder);
		page.setSize(pageSize);
		page.setPageNo(pageNo);
		List<Order> order=orderService.searchOrder(customerId,page,sort);
		session.setAttribute("order",order);
		session.setAttribute("pageNo",pageNo);
		response.sendRedirect("ViewOrder.jsp");
	}
	
	private void viewOrderItem(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
		HttpSession session=request.getSession();
		int orderId=Integer.parseInt(request.getParameter("orderId"));
		List<OrderItem> orderItem=orderService.getAllOrderItem(orderId);
		session.setAttribute("orderItem",orderItem);
		response.sendRedirect("ViewOrderItem.jsp");
	}

	private void removeItem(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		int orderId=Integer.parseInt(request.getParameter("orderId"));
		int itemId=Integer.parseInt(request.getParameter("itemId"));
		orderService.removeItem(orderId,itemId);
		response.sendRedirect("OrderController?flag=viewOrder&pageNo=1");
	}
	
	private void searchOrder(HttpServletRequest request, HttpServletResponse response, ServletContext application) throws SQLException, IOException {
		HttpSession session=request.getSession();
		
		Page page=new Page();
		Sort sort=new Sort();
		
		int customerId=(int)application.getAttribute("searchCustomerId");
		int pageNo=Integer.parseInt(request.getParameter("pageNo"));
		
		String sortFiled=sort.getSortFiled();
		String sortOrder=sort.getSortOrder();
		int pageSize=page.getSize();
		
		if("true".equals(request.getParameter("loaded")))
		{
			sortFiled=request.getParameter("sortFiled");
			sortOrder=request.getParameter("sortOrder");
			
			if(!request.getParameter("pageSize").isEmpty())
				pageSize=Integer.parseInt(request.getParameter("pageSize"));
			session.setAttribute("sortFiled",sortFiled);
			session.setAttribute("sortOrder",sortOrder);
			session.setAttribute("pageSize",pageSize);
		}
		else
		{
			if(session.getAttribute("sortFiled")!=null)
			{
				sortFiled=(String)session.getAttribute("sortFiled");
				sortOrder=(String)session.getAttribute("sortOrder");
				pageSize=(int)session.getAttribute("pageSize");	
			}
		}
		sort.setSortFiled(sortFiled);
		sort.setSortOrder(sortOrder);
		page.setSize(pageSize);
		page.setPageNo(pageNo);
		List<Order> order=orderService.searchOrder(customerId,page,sort);
		session.setAttribute("order",order);
		session.setAttribute("pageNo",pageNo);
		response.sendRedirect("ViewSearchOrder.jsp");
	}
	
	private void searchOrderItem(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		HttpSession session=request.getSession();
		int orderId=Integer.parseInt(request.getParameter("orderId"));
		List<OrderItem> orderItem=orderService.searchOrderItem(orderId);
		session.setAttribute("orderItem",orderItem);
		response.sendRedirect("ViewSearchOrderItem.jsp");
	}
	
	private void cancelOrder(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		int orderId=Integer.parseInt(request.getParameter("orderId"));
		orderService.cancleOrder(orderId);
		response.sendRedirect("OrderController?flag=viewOrder&pageNo=1");
	}


}
