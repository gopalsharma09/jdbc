package com.brevitazproject.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.brevitazproject.model.Customer;
import com.brevitazproject.service.CustomerService;

@WebServlet("/CustomerController")
public class CustomerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private CustomerService customerService=new CustomerService();
    
    public CustomerController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String flag=request.getParameter("flag");
		
		if(flag.equals("registration"))
			response.sendRedirect("Registration.jsp");
		
		else if(flag.equals("customerProfile"))
			response.sendRedirect("CustomerProfile.jsp");
		
		else if(flag.equals("searchCustomer"))
			response.sendRedirect("SearchPage.jsp");
		
		else if(flag.equals("editCustomerDetails"))
		{
			try {
				editCustomerDetails(request,response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("deleteCustomerAccount"))
		{
			try {
				deleteCustomerAccount(request,response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		ServletContext application=getServletContext();
		String flag=request.getParameter("flag");
		
		if(flag.equals("register"))
		{
			try {
				saveCustomerDetails(request,response,application);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("search"))
		{
			try {
				searchCustomer(request,response,application);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(flag.equals("updateCustomer"))
		{
				try {
					updateCustomerDetails(request,response,application);
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	private void searchCustomer(HttpServletRequest request, HttpServletResponse response, ServletContext application) throws SQLException, IOException {
		
		HttpSession session=request.getSession();
		int customerId=Integer.parseInt(request.getParameter("searchCustomerId"));
		boolean customer=customerService.searchCustomer(customerId);
		if(customer==false)
		{
			session.setAttribute("Error","Please Enter Valid Customer Id");
			response.sendRedirect("SearchPage.jsp");
		}
		else
		{
			application.setAttribute("searchCustomerId",customerId);
			response.sendRedirect("OrderController?flag=searchOrder&pageNo=1");
		}
	}

	private void saveCustomerDetails(HttpServletRequest request, HttpServletResponse response, ServletContext application) throws ClassNotFoundException, SQLException, IOException {
		HttpSession session=request.getSession();
		Customer customer=new Customer();
		
		String firstName=request.getParameter("firstName");
		String lastName=request.getParameter("lastName");
		String phoneNumber=request.getParameter("phoneNumber");
		
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setPhoneNumber(phoneNumber);
		List<Customer> customerDetails=customerService.saveCustomerDetails(customer);
		
		session.setAttribute("customerDetails",customerDetails);
		application.setAttribute("customerId",customerDetails.get(0).getCustomerId());
		
		response.sendRedirect("HomePage.jsp");
	}
	
	private void deleteCustomerAccount(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
		int customerId=Integer.parseInt(request.getParameter("customerId"));
		customerService.deleteCustomerAccount(customerId);
		response.sendRedirect("Registration.jsp");
	}

	private void editCustomerDetails(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		HttpSession session=request.getSession();
		int customerId=Integer.parseInt(request.getParameter("customerId"));
		List<Customer> customer=customerService.getCustomerDetails(customerId);
		session.setAttribute("customer",customer);
		response.sendRedirect("UpdateCustomer.jsp");
	}
	
	private void updateCustomerDetails(HttpServletRequest request, HttpServletResponse response, ServletContext application) throws SQLException, IOException {
		Customer customer=new Customer();
		int customerId=(int)application.getAttribute("customerId");
		String firstName=request.getParameter("firstName");
		String lastName=request.getParameter("lastName");
		String phoneNumber=request.getParameter("phoneNumber");
		
		customer.setCustomerId(customerId);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setPhoneNumber(phoneNumber);
		
		customerService.updateCustomerDetails(customer);
		response.sendRedirect("Registration.jsp");
	}


}
