package com.brevitazproject.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.brevitazproject.model.Item;
import com.brevitazproject.model.Order;
import com.brevitazproject.model.OrderItem;
import com.brevitazproject.model.Page;
import com.brevitazproject.model.Sort;

public class OrderDao {

	private ConnectToDataBase connectToDataBase=new ConnectToDataBase();
	
	public List<Item> getMenu() throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		List<Item> listOfItem=new ArrayList<>();
		ResultSet resultSet=statement.executeQuery("select * from menu");
		while(resultSet.next())
		{
			Item item=new Item();
			item.setItemId(resultSet.getInt("id"));
			item.setItemName(resultSet.getString("item_name"));
			item.setItemPrice(resultSet.getInt("item_price"));
			listOfItem.add(item);
		}
		return listOfItem;
	}
	public Item getItemDetails(Item item) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		ResultSet resultSet=statement.executeQuery("select * from menu where id='"+item.getItemId()+"'");
		while(resultSet.next())
		{
			item.setItemId(resultSet.getInt("id"));
			item.setItemName(resultSet.getString("item_name"));
			item.setItemPrice(resultSet.getInt("item_price"));
		}
		return item;
	}
	
	public void saveOrder(Order order) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		statement.execute("insert into order_details (order_date,customer_id) values('"+order.getDate()+"','"+order.getCustomer().getCustomerId()+"')");
	}
	
	public void saveOrderDetails(List<OrderItem> orderItems) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		for(OrderItem orderItem:orderItems)
		{
			statement.execute("insert into order_item(item_name,item_price,quantity,order_id) "
				+ "values('"+orderItem.getItemName()+"','"+orderItem.getItemPrice()+"','"+orderItem.getQuantity()+"','"+orderItem.getOrder().getOrderId()+"')");
		}
	}
	
	public Order getOrder(Order order) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		ResultSet resultSet=statement.executeQuery("select order_id from order_details where customer_id='"+order.getCustomer().getCustomerId()+"' AND order_date='"+order.getDate()+"'");
		while(resultSet.next()){
			order.setOrderId(resultSet.getInt("order_id"));
		}
		return order;
	}
	
	public List<Order> getAllOrder(int customerId, Page page, Sort sort) throws SQLException {
		List<Order> listOfOrder=new ArrayList<>();
		Statement statement=connectToDataBase.getStatement();
		int offSet=(page.getPageNo()-1)*page.getSize();
		String query="select * from order_details where customer_id="+customerId+" ORDER BY "+sort.getSortFiled()+" "+sort.getSortOrder()+" LIMIT "+page.getSize()+" OFFSET "+offSet+"";
		ResultSet resultSet=statement.executeQuery(query);
		while(resultSet.next()){
			Order order=new Order();
			order.setOrderId(resultSet.getInt("order_id"));
			order.setDate(resultSet.getDate("order_date"));
			listOfOrder.add(order);
		}
		return listOfOrder;
	}
	
	public List<OrderItem> getAllOrderItem(int orderId) throws SQLException {
		List<OrderItem> listOfOrderItem=new ArrayList<>();
		Statement statement=connectToDataBase.getStatement();
		ResultSet resultSet=statement.executeQuery("select * from order_item where order_id='"+orderId+"'");
		while(resultSet.next()){
			OrderItem orderItem=new OrderItem();
			Order order=new Order();
			order.setOrderId(orderId);
			orderItem.setItemId(resultSet.getInt("id"));
			orderItem.setItemName(resultSet.getString("item_name"));
			orderItem.setItemPrice(resultSet.getInt("item_price"));
			orderItem.setQuantity(resultSet.getInt("quantity"));
			orderItem.setOrder(order);
			listOfOrderItem.add(orderItem);
		}
		return listOfOrderItem;	
	}
	
	public List<Order> searchOrder(int customerId, Page page, Sort sort) throws SQLException {
		List<Order> listOfOrder=new ArrayList<>();
		Statement statement=connectToDataBase.getStatement();
		int offSet=(page.getPageNo()-1)*page.getSize();
		String query="select * from order_details where customer_id="+customerId+" ORDER BY "+sort.getSortFiled()+" "+sort.getSortOrder()+" LIMIT "+page.getSize()+" OFFSET "+offSet+"";
		ResultSet resultSet=statement.executeQuery(query);
		while(resultSet.next()){
			Order order=new Order();
			order.setOrderId(resultSet.getInt("order_id"));
			order.setDate(resultSet.getDate("order_date"));
			listOfOrder.add(order);
		}
		return listOfOrder;
	}
	public List<OrderItem> searchOrderItem(int orderId) throws SQLException {
		List<OrderItem> listOfOrderItem=new ArrayList<>();
		Statement statement=connectToDataBase.getStatement();
		ResultSet resultSet=statement.executeQuery("select * from order_item where order_id='"+orderId+"'");
		while(resultSet.next()){
			OrderItem orderItem=new OrderItem();
			Order order=new Order();
			order.setOrderId(orderId);
			orderItem.setItemId(resultSet.getInt("id"));
			orderItem.setItemName(resultSet.getString("item_name"));
			orderItem.setItemPrice(resultSet.getInt("item_price"));
			orderItem.setQuantity(resultSet.getInt("quantity"));
			orderItem.setOrder(order);
			listOfOrderItem.add(orderItem);
		}
		return listOfOrderItem;
	}
	
	public void deleteItem(int orderId, int itemId) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		statement.executeUpdate("delete from order_item where order_id='"+orderId+"' AND id='"+itemId+"'");
	}
	
	
	public void deleteOrder(int orderId) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		statement.executeUpdate("delete from order_item where order_id='"+orderId+"'");
		statement.executeUpdate("delete from order_details where order_id='"+orderId+"'");
	}
	
}
