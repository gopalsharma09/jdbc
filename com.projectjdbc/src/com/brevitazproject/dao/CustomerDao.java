package com.brevitazproject.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.brevitazproject.model.Customer;

public class CustomerDao {
	
	private ConnectToDataBase connectToDataBase=new ConnectToDataBase();
	
	public void saveCustomer(Customer customer) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		statement.execute("insert into customer(first_name,last_name,phone_number) "
				+ "values ('"+customer.getFirstName()+"','"+customer.getLastName()+"','"+customer.getPhoneNumber()+"')");
	}

	public Customer getCustomer(Customer customer) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		ResultSet resultSet=statement.executeQuery("select * from customer where first_name='"+customer.getFirstName()+"' AND last_name='"+customer.getLastName()+"' AND phone_number='"+customer.getPhoneNumber()+"'");
		while(resultSet.next())
		{
			customer.setCustomerId(resultSet.getInt("customer_id"));
			customer.setFirstName(resultSet.getString("first_name"));
			customer.setLastName(resultSet.getString("last_name"));
			customer.setPhoneNumber(resultSet.getString("phone_number"));
		}
		return customer;
	}

	public boolean authenticateCustomer(Customer customer) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		ResultSet resultSet=statement.executeQuery("select * from customer where first_name='"+customer.getFirstName()+"' AND last_name='"+customer.getLastName()+"' AND phone_number='"+customer.getPhoneNumber()+"'");
		return resultSet.next();	
	}

	public boolean searchCustomer(int customerId) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		ResultSet resultSet=statement.executeQuery("select * from customer where customer_id='"+customerId+"'");
		return resultSet.next();
	}

	public void deleteCustomerDetails(int customerId) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		statement.executeUpdate("delete from order_item where order_id IN "
				+ "(select order_id from order_details where customer_id='"+customerId+"')");
		statement.executeUpdate("delete from order_details where customer_id='"+customerId+"'");
		statement.executeUpdate("delete from customer where customer_id='"+customerId+"'");
	}

	public List<Customer> getCustomerDetails(int customerId) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		List<Customer> customerDetails=new ArrayList<>();
		ResultSet resultSet=statement.executeQuery("select * from customer where customer_id='"+customerId+"'");
		while(resultSet.next())
		{
			Customer customer=new Customer();
			customer.setCustomerId(resultSet.getInt("customer_id"));
			customer.setFirstName(resultSet.getString("first_name"));
			customer.setLastName(resultSet.getString("last_name"));
			customer.setPhoneNumber(resultSet.getString("phone_number"));
			customerDetails.add(customer);
		}
		return customerDetails;
	}

	public void updateCustomerDetails(Customer customer) throws SQLException {
		Statement statement=connectToDataBase.getStatement();
		statement.executeUpdate("update customer set first_name='"+customer.getFirstName()+"',"
				+ "last_name='"+customer.getLastName()+"',phone_number='"+customer.getPhoneNumber()+"'"
						+ " where customer_id='"+customer.getCustomerId()+"'");
		
	}
}
