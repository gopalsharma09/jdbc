package com.brevitazproject.model;

import java.sql.Date;
import java.util.List;

public class Order {

	private int orderId;
	private Customer customer;
	private Date date;
	private List<OrderItem> listOfOrderItem;
	
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public List<OrderItem> getListOfOrderItem() {
		return listOfOrderItem;
	}
	public void setListOfOrderItem(List<OrderItem> listOfOrderItem) {
		this.listOfOrderItem = listOfOrderItem;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
}
